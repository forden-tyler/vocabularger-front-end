'use strict';

describe('Directive: jumbledWord', function () {

  // load the directive's module
  beforeEach(module('vocabulargerApp'));

  var element,
    scope;

  beforeEach(inject(function ($rootScope) {
    scope = $rootScope.$new();
  }));

  it('should make hidden element visible', inject(function ($compile) {
    element = angular.element('<jumbled-word></jumbled-word>');
    element = $compile(element)(scope);
    expect(element.text()).toBe('this is the jumbledWord directive');
  }));
});
