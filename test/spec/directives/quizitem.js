'use strict';

describe('Directive: quizItem', function () {

  // load the directive's module
  beforeEach(module('vocabulargerApp'));

  var element,
    scope;

  beforeEach(inject(function ($rootScope) {
    scope = $rootScope.$new();
  }));

  it('should make hidden element visible', inject(function ($compile) {
    element = angular.element('<quiz-item></quiz-item>');
    element = $compile(element)(scope);
    expect(element.text()).toBe('this is the quizItem directive');
  }));
});
