'use strict';

describe('Service: vocabularyItemService', function () {

  // load the service's module
  beforeEach(module('vocabulargerApp'));

  // instantiate service
  var vocabularyItemService;
  beforeEach(inject(function (_vocabularyItemService_) {
    vocabularyItemService = _vocabularyItemService_;
  }));

  it('should do something', function () {
    expect(!!vocabularyItemService).toBe(true);
  });

});
