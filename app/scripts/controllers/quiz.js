'use strict';

/**
 * @ngdoc function
 * @name vocabulargerApp.controller:QuizCtrl
 * @description
 * # QuizCtrl
 * Controller of the vocabulargerApp
 */
angular.module('vocabulargerApp')
  .controller('QuizCtrl', ['$scope', '$timeout', 'vocabularyItemService', function ($scope, $timeout, vocabularyItemService) {
    $scope.userAnswer = '';
    $scope.answeredIncorrectly = false;
    $scope.inputClass = '';
    
    $scope.submitAnswer = submitAnswer;
    $scope.hintLetterClick = hintLetterClick;
    
    activate();

    function activate() {
        return vocabularyItemService.getVocabularyItemsInReview().then(function(items) {
          if (items.length > 0) {
              $scope.items = items;
              $scope.currentItem = items[0];
              $scope.imageSource = "http://localhost:51836/api/imagefiles/" + $scope.currentItem.imageId;
          }
        });
    }
    
    function hintLetterClick(e) {
      if ($scope.userAnswer) {
        $scope.userAnswer += e.data.letter;
      } else {
        $scope.userAnswer = e.data.letter; 
      }
       
      $scope.$apply();
    }
	
	function submitAnswer(answer) {
		var item = $scope.currentItem;
      
        //If answered correctly
		if (answer.toUpperCase() === item.answer.toUpperCase()) {
          $scope.inputClass = 'vi-input-correct-animate';
          responsiveVoice.speak(answer, "Indonesian Female");
          
          //Never answered incorrectly and no hints were used
          if (!$scope.answeredIncorrectly && !$scope.hintUsed) {
            if (item.knowledgeLevel < 6) {
              item.knowledgeLevel++; 
            }
          } else if (item.knowledgeLevel > 0) {
            item.knowledgeLevel--;
          }
          
          item.lastReviewed = new Date(Date.now() + 60*60*1000);
          item.numberOfReviews++;

          $timeout(function() {
            vocabularyItemService.updateVocabularyItem(item).then(nextItem);
          }, 1000);    
		}
		else {
          $scope.answeredIncorrectly = true;
          $scope.inputClass = 'vi-input-incorrect-animate';
          $timeout(function() {$scope.inputClass = ''}, 1000);
		}
	}
    
    function nextItem() {
      $scope.userAnswer = '';
      $scope.answeredIncorrectly = false;
      $scope.inputClass = '';

      $scope.items = $scope.items.slice(1, $scope.items.length);
      
      if ($scope.items[0]) {
        $scope.currentItem = $scope.items[0];
        $scope.imageSource = "http://localhost:51836/api/imagefiles/" + $scope.currentItem.imageId;
      }
    }
  }]);
