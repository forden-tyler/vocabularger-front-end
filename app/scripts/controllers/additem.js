'use strict';

/**
 * @ngdoc function
 * @name vocabulargerApp.controller:AdditemCtrl
 * @description
 * # AdditemCtrl
 * Controller of the vocabulargerApp
 */
angular.module('vocabulargerApp')
  .controller('AddItemCtrl', ['$scope', 'vocabularyItemService', function ($scope, vocabularyItemService) {
  
    
    $scope.createVocabularyItem = function (item) {
		item.knowledgeLevel = 1;
		item.lastReviewed = new Date(0);
		item.numberOfReviews = 0;
      
        vocabularyItemService.uploadImage("name", $scope.file).then(function(result) {
            item.imageId = result.id;
            vocabularyItemService.createVocabularyItem(item).then(function() {
                alert('Item successfully added!');
                item.display = '';
                item.answer = '';
                item.example = '';	
                $scope.file = undefined;
              },
              function(error) {
                alert('An error has occured: ' + error);
              }
            );
        });
	}
  }]);
