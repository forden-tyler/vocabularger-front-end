'use strict';

/**
 * @ngdoc function
 * @name vocabulargerApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the vocabulargerApp
 */
angular.module('vocabulargerApp')
  .controller('AboutCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
