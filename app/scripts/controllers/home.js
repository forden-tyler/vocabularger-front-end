'use strict';

/**
 * @ngdoc function
 * @name vocabulargerApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the vocabulargerApp
 */
angular.module('vocabulargerApp')
  .controller('HomeCtrl', ['$scope', 'vocabularyItemService', function ($scope, vocabularyItemService) {
    $scope.numInReview = 6;
    $scope.totalItems = 100;
    $scope.total1 = 5;
    $scope.total2 = 15;
    $scope.total3 = 20;
    $scope.total4 = 32;
    $scope.total5 = 18;
    $scope.total6 = 10;
  }]);
