'use strict';

/**
 * @ngdoc function
 * @name vocabulargerApp.controller:NavbarCtrl
 * @description
 * # NavbarCtrl
 * Controller of the vocabulargerApp
 */
angular.module('vocabulargerApp')
  .controller('NavbarCtrl', [
    '$scope',
    '$state',
    '$rootScope',
    function ($scope, $state, $rootScope) {
      /**************
       * SCOPE DATA *
       **************/
      $scope.currentStateName = $state.current.name;

      $scope.navBarItems = [
        {
          sref: 'root.quiz',
          title: 'Test Me',
          viewable: function () {
            return true;
          }
        },
        {
          sref: 'root.additem',
          title: 'Add Vocabulary',
          viewable: function () {
            return true;
          }
        }
      ];

      $scope.navBarTasks = [
        {
          style: 'cursor: pointer;',
          title: 'Logout',
          clicked: function () {
            $state.go('login');
          },
          viewable: function () {
            return true;
          }
        }
      ];

      /******************
       * EVENT HANDLERS *
       ******************/
      // When the page changes, update the current state name
      $rootScope.$on('$stateChangeStart', function (event, toState, toParams, fromState, fromParams) { // jshint ignore:line
        $scope.currentStateName = toState.name;
      });
  }]);
