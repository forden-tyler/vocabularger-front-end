'use strict';

/**
 * @ngdoc function
 * @name vocabulargerApp.controller:LoginCtrl
 * @description
 * # LoginCtrl
 * Controller of the vocabulargerApp
 */
angular.module('vocabulargerApp')
  .controller('LoginCtrl',['$scope', '$state', function ($scope, $state) {
    $scope.login = login;
    
    function login() {
      $state.go('root.home');
    }
  }]);
