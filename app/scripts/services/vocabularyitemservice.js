'use strict';

/**
 * @ngdoc service
 * @name vocabulargerApp.vocabularyItemService
 * @description
 * # vocabularyItemService
 * Service in the vocabulargerApp.
 */
angular.module('vocabulargerApp')
  .service('vocabularyItemService', [
    'httpService',
    '$q',
    function (HttpService, $q) {
		
	  var location = "http://localhost:51836";

      this.getVocabularyItem = function (id) {
        return HttpService.get(location + '/api/vocabularyitems/' + id, null,
          function (response) {
            return response.data;
          },
          function (response) {
            return $q.reject(response.statusText);
          });
      };

      /*
       * Returns the number of recipients with the specified status and sendJob
       */
      this.getVocabularyItemsInReview = function () {
        return HttpService.get(location + '/api/vocabularyitems/', null,
          function (response) {
            return response.data;
          },
          function (response) {
            return $q.reject(response.statusText);
          });
      };

      this.createVocabularyItem = function (vocabularyItem) {
        return HttpService.post(location + '/api/vocabularyitems/', vocabularyItem, null,
          function (response) {
            return response.data;
          },
          function (response) {
            return $q.reject(response.statusText);
          });
      };
	  
	  this.updateVocabularyItem = function (vocabularyItem) {
        return HttpService.put(location + '/api/vocabularyitems/' + vocabularyItem.id, vocabularyItem, null,
          function (response) {
            return response.data;
          },
          function (response) {
            return $q.reject(response.statusText);
          });
      };

      this.deleteVocabularyItem = function (id) {
        return HttpService.delete(location + '/api/vocabularyitems/' + id, null,
          function (response) {
            return response;
          },
          function (response) {
            return $q.reject(response.statusText);
          });
      };
      
      this.uploadImage = function (name, data) {
       return HttpService.post(location + '/api/imagefiles/', data, {
         headers: {
           'Content-Type': undefined
         }
        },
        function (response) {
          return response.data;
        },
        function (response) {
          return $q.reject(response.statusText);
        });
      };
  }]);
