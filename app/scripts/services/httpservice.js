'use strict';

/**
 * @ngdoc service
 * @name vocabulargerApp.httpService
 * @description
 * # httpService
 * Service in the vocabulargerApp.
 */
angular.module('vocabulargerApp')
  .service('httpService', ['$http', '$q', function ($http, $q) {

    function addAbort (promise, deferredAbort) {
      promise.abort = function () {
        deferredAbort.resolve();
      };

      promise.finally(
        function () {
          promise.abort = angular.noop;
          deferredAbort = promise = null;
        }
      );
	}
	 
    this.get = function(url, options, successFunction, failureFunction) {
      var deferredAbort = $q.defer();
      var newOptions;
      
      if (options == null) {
        options = {};
      }
      options.timeout = deferredAbort.promise
      
      var promise = $http.get(url, options).then(successFunction, failureFunction);
      
      addAbort(promise, deferredAbort);
      
      return promise;
    }
    
    this.post = function(url, requestBody, options, successFunction, failureFunction) {
      var deferredAbort = $q.defer();
      
      if (options == null) {
        options = {};
      }
      options.timeout = deferredAbort.promise
      
      var promise = $http.post(url, requestBody, options).then(successFunction, failureFunction);
      
      addAbort(promise, deferredAbort);
      
      return promise;
    }

    this.delete = function(url, options, successFunction, failureFunction) {
      var deferredAbort = $q.defer();
      
      if (options == null) {
        options = {};
      }
      options.timeout = deferredAbort.promise
      
      var promise = $http.delete(url, options).then(successFunction, failureFunction);
      
      addAbort(promise, deferredAbort);
      
      return promise;
    }
	
    this.put = function(url, requestBody, options, successFunction, failureFunction) {
      var deferredAbort = $q.defer();
      
      if (options == null) {
        options = {};
      }
      options.timeout = deferredAbort.promise
      
      var promise = $http.put(url, requestBody, options).then(successFunction, failureFunction);
      
      addAbort(promise, deferredAbort);
      
      return promise;
    }

  }]);
