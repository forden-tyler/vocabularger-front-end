'use strict';

/**
 * @ngdoc directive
 * @name vocabulargerApp.directive:jumbledWord
 * @description
 * # jumbledWord
 */
angular.module('vocabulargerApp')
  .directive('jumbledWord', function () {
    return {
      scope: {
        word: '=',
        active: '=',
        input: '=',
        letterClick: '&'
      },
      template: '',
      restrict: 'E',
      link: function postLink(scope, element, attrs) {
        scope.createJumbledWord = createJumbledWord;

        scope.$watch('word', initialise);
        scope.$watch('input', inputUpdate);

        scope.onLetterClick = onLetterClick;


        function onLetterClick(letter) {
          scope.letterClick({letter: letter});
        }          

        function initialise() {
          scope.active = false;
          element.empty();
          element.append('<button type="button" class="btn btn-info" ng-click="createJumbledWord()">Show Hint</button>');
          element.on('click', createJumbledWord);
        }
        
        function inputUpdate(input) {
          if(scope.active) {
            var buttons = element.children().removeAttr('disabled');
            /*for(var i = 0; i < buttons.length; i++) {
              buttons[i].setAttribute('disabled', 'false'); 
            }*/
            for(var i = 0; i < input.length; i++) {
              buttons = element.children('[letter="'+input[i]+'"]:enabled');
              if (buttons.length > 0) {
                buttons[0].setAttribute('disabled', 'true'); 
              }
            }
          }
        }
        
        function createJumbledWord() {
          element.empty();
          element.off('click');
          scope.active = true;
          var jumbledWord = shuffle(scope.word);

          for(var i = 0; i < jumbledWord.length; i++) {
            var ltrBtn = $('<button type="button" class="btn btn-default jumbled-letter" letter="' + jumbledWord[i] + '">' + jumbledWord[i] + '</button>');

            ltrBtn.on('click', {letter: jumbledWord[i]}, scope.onLetterClick);

            element.append(ltrBtn);
          }
          inputUpdate(scope.input);
        }
        
          function shuffle(str) {
           var a, n;
           do {
            a = str.split("")
            n = a.length;

            for(var i = n - 1; i > 0; i--) {
                var j = Math.floor(Math.random() * (i + 1));
                var tmp = a[i];
                a[i] = a[j];
                a[j] = tmp;
            }
           } while (a.join("") == str);
            return a.join("");
          }
        }
    };
  });
