'use strict';

/**
 * @ngdoc directive
 * @name vocabulargerApp.directive:verticalProgressBar
 * @description
 * # verticalProgressBar
 */
angular.module('vocabulargerApp')
  .directive('verticalProgressBar', function () {
    return {
      scope: {
        total: '=',
        current: '=',
        width: '=',
        height: '=',
        label: '='
      },
      template: '<div class="vertical-progress"><div class="vertical-progress-bar"><span class="vertical-progress-text">{{label}}</span></div></div>',
      restrict: 'E',
      link: function postLink(scope, element, attrs) {
        var height, container, bar;
        
        height = scope.height ? scope.height : element.height;
        container = element.children('.vertical-progress');
        bar = container.children('.vertical-progress-bar');
        
        container.css('height', height + 'px');
        bar.css('line-height', height + 'px');
//        element.width = scope.width ? scope.width : element.width;
        
        scope.$watch('current', updateProgress);
        
        updateProgress();
        
        function updateProgress(current) {
          element.children().children().css('height', (100 - (current/scope.total)*100) + '%')
        }
      }
    };
  });
