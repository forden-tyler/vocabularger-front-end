'use strict';

/**
 * @ngdoc directive
 * @name vocabulargerApp.directive:quizItem
 * @description
 * # quizItem
 */
angular.module('vocabulargerApp')
  .directive('quizItem', function () {
    return {
      template: '<div><input type="text" class="form-"></div>',
      restrict: 'E',
      link: function postLink(scope, element, attrs) {
        element.text('this is the quizItem directive');
      }
    };
  });
