'use strict';

/**
 * @ngdoc overview
 * @name vocabulargerApp
 * @description
 * # vocabulargerApp
 *
 * Main module of the application.
 */
angular
  .module('vocabulargerApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch',
    'ui.router'
  ])
  .config([
    '$stateProvider',
    '$urlRouterProvider',
    '$httpProvider',
    function ($stateProvider, $urlRouterProvider, $httpProvider) {

      $stateProvider
        .state('login', {
          url: '/login',
          templateUrl: 'login.html',
          controller: 'LoginCtrl'
        })
        .state('root', {
          abstract: true,
          templateUrl: 'views/root.html'
        })
        .state('root.home', {
          url: '/home',
          templateUrl: 'views/home.html',
          controller: 'HomeCtrl'
        })
        .state('root.quiz', {
          url: '/quiz',
          templateUrl: 'views/quiz.html',
          controller: 'QuizCtrl'
        })
        .state('root.additem', {
          url: '/additem',
          templateUrl: 'views/additem.html',
          controller: 'AddItemCtrl'
        });
  
      $urlRouterProvider.otherwise('/home');

      /* Enable Cookies and CORS */
//      $httpProvider.defaults.withCredentials = true;
//      $httpProvider.defaults.useXDomain = true;
          
//	$locationProvider.html5Mode(false).hashPrefix('');
//	
//    $routeProvider
//      .when('/', {
//        templateUrl: 'views/main.html',
//        controller: 'MainCtrl',
//        controllerAs: 'main'
//      })
//      .when('/about', {
//        templateUrl: 'views/about.html',
//        controller: 'AboutCtrl',
//        controllerAs: 'about'
//      })
//	  .when('/quiz', {
//        templateUrl: 'views/quiz.html',
//        controller: 'QuizCtrl',
//        controllerAs: 'quiz'
//      })
//	  .when('/additem', {
//        templateUrl: 'views/additem.html',
//        controller: 'AddItemCtrl',
//        controllerAs: 'additem'
//      })
//      .otherwise({
//        redirectTo: '/'
//      });
  }]);
